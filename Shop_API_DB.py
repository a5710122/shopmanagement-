
import sqlite3

def create_db(self):
     """ Create database (SQL) """

     # Connect to Database
     conn = sqlite3.connect('test.db')
     print("Open Success")
     # Execute command SQL
     conn.execute('''CREATE TABLE PRODUCT
     (ID INTEGER PRIMARY KEY AUTOINCREMENT,
     NAME           TEXT    NOT NULL,
     STOCK           INT     NOT NULL,
     PRICE           INT     NOT NULL);''')
     print("Success")
     conn.close()
