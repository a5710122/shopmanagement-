from tkinter import *
from tkinter.ttk import *
from tkinter import messagebox
import sqlite3

class Application(Frame):
  def __init__(self, master):
    self.id = []
    self.name = []
    self.stock = []
    self.price = []
    # initialize frame
    Frame.__init__(self, master)
    self.grid()
    self.create_widgets_product()
    self.create_widgets_search()
    self.create_widgets_show_product()
    self.create_widgets_update()
    self.delete_widgets_product()


  def create_widgets_product(self):
    """ Create widgets produect """

    # add label frame for Product
    self.label1 = LabelFrame(self, text="Add Product")
    self.label1.grid(row=0, column=0, sticky='W', \
    padx=5, pady=5, ipadx=5, ipady=5)

    # add label for Add Product
    self.label_product = Label(self.label1, width=15)
    self.label_product.grid(column=0, row=2, padx=5, pady=5)
    self.label_product["text"] = "Product :"

    # add Entry (EditText) to Add Product
    self.entry_product = Entry(self.label1, width=50)
    self.entry_product.grid(column=1, row=2, padx=5, pady=5)

    # add label for Add Stock
    self.label_stock = Label(self.label1, width=15)
    self.label_stock.grid(column=0, row=3, sticky='W', padx=5, pady=5)
    self.label_stock["text"] = "Stock :"

    # add Entry (EditText) to Add Stock
    self.entry_stock = Entry(self.label1, width=10)
    self.entry_stock.grid(column=1, row=3, sticky='W', padx=5, pady=5)

    # add label for price product
    self.label_price = Label(self.label1, width=20)
    self.label_price.grid(column=1, row=3, sticky='E',  padx=5, pady=5)
    self.label_price["text"] = "Price :"

    # add Entry (EditText) to price product
    self.entry_price = Entry(self.label1, width=10)
    self.entry_price.grid(column=1, row=3, sticky='E', padx=5, pady=5)

    # create Done button for add product
    self.button = Button(self)
    self.button["text"] = "Add"
    self.button["command"] = self.add_product
    self.button.grid(row=0, column=1)


  def create_widgets_search(self):
    """ Create widgets search """

    # add label frame for Search
    self.label2 = LabelFrame(self, text="Search")
    self.label2.grid(row=4, column=0, sticky='WE', \
    padx=5, pady=5, ipadx=5, ipady=5)

    # add label for search Product
    self.label_search = Label(self.label2, width=15)
    self.label_search.grid(column=0, row=2, sticky='E', padx=5, pady=5)
    self.label_search["text"] = "Search product :"

    # add Entry (EditText) to search Product
    self.entry_search = Entry(self.label2, width=50)
    self.entry_search.grid(column=1, row=2, sticky='E', padx=5, pady=5)

    # create Search button for search product
    self.button = Button(self)
    self.button["text"] = "Search"
    self.button["command"] = self.print_search
    self.button.grid(column=1, row=4)


  def create_widgets_show_product(self):
    """ Create widgets show product """

    # add label frame for Show product
    self.label = LabelFrame(self, text="Product")
    self.label.grid(row=8, column=0, sticky='WE', \
    padx=5, pady=5, ipadx=5, ipady=5)

    number = 1

    # row Product
    self.label_row = Label(self.label, width=15)
    self.label_row.grid(column=0, row=1 + number, sticky='E', padx=5, pady=5)
    self.label_row["text"] = "ID"

    # row Product
    self.label_row = Label(self.label, width=15)
    self.label_row.grid(column=1, row=1 + number, sticky='E', padx=5, pady=5)
    self.label_row["text"] = "Product"

    # row Stock
    self.label_column = Label(self.label, width=15)
    self.label_column.grid(column=2, row=1 + number, sticky='E', padx=5, pady=5)
    self.label_column["text"] = "Stock"

    # row price
    self.label_column = Label(self.label, width=15)
    self.label_column.grid(column=3, row=1 + number, sticky='E', padx=5, pady=5)
    self.label_column["text"] = "Price"

    self.selete_data()
    # Loop query (show) data in database
    for count in range(len(self.name)):
        print(self.id)
        self.label_product = Label(self.label, width=15)
        self.label_product.grid(column=0, row=3 + number)
        self.label_product["text"] = self.id[count]

        self.label_product = Label(self.label, width=15)
        self.label_product.grid(column=1, row=3 + number)
        self.label_product["text"] = self.name[count]

        self.label_stock = Label(self.label, width=15)
        self.label_stock.grid(column=2, row=3 + number)
        self.label_stock["text"] = self.stock[count]

        self.label_stock = Label(self.label, width=15)
        self.label_stock.grid(column=3, row=3 + number)
        self.label_stock["text"] = self.price[count]
        number = number + 1

    # create Search button for search product
    self.button = Button(self)
    self.button["text"] = "Refresh"
    self.button["command"] = self.create_widgets_show_product
    self.button.grid(column=0, row=11 )

  def delete_widgets_product(self):
      """ Delete widgets produect """

      # add label frame for Product
      self.label = LabelFrame(self, text="Delete Product")
      self.label.grid(row=6, column=0, sticky='WE', \
      padx=5, pady=5, ipadx=5, ipady=5)

      # add label for delete Product
      self.label_delete = Label(self.label, width=15)
      self.label_delete.grid(column=0, row=2, sticky='E', padx=5, pady=5)
      self.label_delete["text"] = "Delete :"

      # add Entry (EditText) to Delete Product
      self.entry_delete = Entry(self.label, width=50)
      self.entry_delete.grid(column=1, row=2, sticky='E', padx=5, pady=5)

      #create Done button for add product
      self.button = Button(self)
      self.button["text"] = "Delete"
      self.button["command"] = self.delete
      self.button.grid(column=1, row=6)

  def create_widgets_update(self):
      """ Create widgets update product """

      # add label frame for update
      self.label = LabelFrame(self, text="Update")
      self.label.grid(row=7, column=0, sticky='WE', \
      padx=5, pady=5, ipadx=5, ipady=5)

      # add label for update Product
      self.label_product_update = Label(self.label, width=15)
      self.label_product_update.grid(column=0, row=2, sticky='W', padx=5, pady=5)
      self.label_product_update["text"] = "Update product :"

      # add Entry (EditText) to update Product
      self.entry_product_update = Entry(self.label, width=50)
      self.entry_product_update.grid(column=1, row=2, sticky='E', padx=5, pady=5)

      # add label for update Stock
      self.label_stock_update = Label(self.label, width=15)
      self.label_stock_update.grid(column=0, row=3, sticky='W', padx=5, pady=5)
      self.label_stock_update["text"] = "Stock :"

      # add Entry (EditText) to update Stock
      self.entry_stock_update = Entry(self.label, width=10)
      self.entry_stock_update.grid(column=1, row=3, sticky='W', padx=5, pady=5)

      # add label for update price product
      self.label_price_update = Label(self.label, width=20)
      self.label_price_update.grid(column=1, row=3, sticky='E',  padx=5, pady=5)
      self.label_price_update["text"] = "Price :"

      # add Entry (EditText) to update price product
      self.entry_price_update = Entry(self.label, width=10)
      self.entry_price_update.grid(column=1, row=3, sticky='E', padx=5, pady=5)

      # add label for update id product
      self.label_id = Label(self.label, width=20)
      self.label_id.grid(column=0, row=4, sticky='W',  padx=5, pady=5)
      self.label_id["text"] = "ID :"

      # add Entry (EditText) to update id product
      self.entry_id = Entry(self.label, width=10)
      self.entry_id.grid(column=1, row=4, sticky='W', padx=5, pady=5)

      # create Search button for search product
      self.button = Button(self)
      self.button["text"] = "Update"
      self.button["command"] = self.update
      self.button.grid(column=1, row=7)

  def add_product(self):
      """ Add product take input from create_widgets_product """

      # Check input is not empty
      if self.entry_product.get() != "" and self.entry_stock.get() != "" and self.entry_price.get() != "":
         # Check product duplicate
         if self.entry_product.get() in self.name :
             messagebox.showerror("Error", "Duplicate product")
         else:
           # Send input to method manage add input to database (insert_data())
           self.insert_data(self.entry_product.get(), self.entry_stock.get(), self.entry_price.get())
           messagebox.showinfo("Success", "Add Product: " + self.entry_product.get() + "have stock: " + self.entry_stock.get() + " Success!!")
           self.entry_product.delete(0, 'end')
           self.entry_stock.delete(0, 'end')
           self.entry_price.delete(0, 'end')
      else:
         messagebox.showerror("Error", "Error please check your product input")


  def print_search(self):
      """ Search product take input from create_widgets_search """

      # Check input is not empty
      if self.entry_search.get() != "" :
         # Find input
         if self.entry_search.get() in self.name :
            messagebox.showinfo("Success", "Found your item: " + self.entry_search.get() + " Have Stock: " + str(self.stock[self.name.index(self.entry_search.get())]))
         else:
            messagebox.showerror("Error", "Not Found your item")
         self.entry_search.delete(0, 'end')
      else:
         messagebox.showerror("Error", "Error please check your search input")

  def delete(self):
      """ Delete product take input from delete_widgets_product """

      # Check input is not empty
      if self.entry_delete.get() != "" :
         # Find input
         if self.entry_delete.get() in self.name :
            # Send input to method manage delete data from database (delete_data())
            self.delete_data(self.entry_delete.get())
            messagebox.showinfo("Success", "Delete your item")
            self.entry_delete.delete(0, 'end')
         else:
            messagebox.showerror("Error", "Not Found your item")
      else:
         messagebox.showerror("Error", "Error please check your delete input")

  def update(self):
      """ Edit product take input from create_widgets_update """

      # Convert input to form intiger
      id = int(self.entry_id.get())
      # Find input
      if id in self.id :
        # Send input to method manage update data in database (update_data())
        self.update_data(self.entry_id.get(), self.entry_product_update.get(), self.entry_stock_update.get(), self.entry_price_update.get())
        self.entry_id.delete(0, 'end')
        self.entry_product_update.delete(0, 'end')
        self.entry_stock_update.delete(0, 'end')
        self.entry_price_update.delete(0, 'end')
        messagebox.showinfo("Success", "Success update your item")
      else:
        messagebox.showerror("Error", "Not Found your item")

  def insert_data(self,name, stock, price):
      """ Manage add input to database (SQL) """

      # Connect to Database
      conn = sqlite3.connect('test.db')
      # Execute command SQL
      conn.execute("INSERT INTO PRODUCT (NAME,STOCK,PRICE) VALUES (?, ?, ?)", (name, stock, price))
      # Save operation
      conn.commit()
      conn.close()

  def selete_data(self):
      """ Manage show or query data from database (SQL) """

      # Connect to Database
      conn = sqlite3.connect('test.db')
      with conn:
         cur = conn.cursor()
         # Execute command SQL
         cur.execute("SELECT * FROM PRODUCT")
         # Take data from the database to the variable
         rows = cur.fetchall()
         self.id = []
         self.name = []
         self.stock = []
         self.price = []
         # Take data from the variable to the list
         for row in rows:
            self.id.append(row[0])
            self.name.append(row[1])
            self.stock.append(row[2])
            self.price.append(row[2])

  def delete_data(self, name_product):
      """ Manage delete data from database (SQL) """

      # Connect to Database
      conn =  sqlite3.connect('test.db')
      cur = conn.cursor()
      # Convert input to form string
      conn.text_factory = str
      data3 = str(name_product)
      # Execute command SQL
      cur.execute("DELETE FROM PRODUCT WHERE NAME = '%s';" % data3.strip())
      conn.commit()
      conn.close()

  def update_data(self, id, name, stock, price):
      """ Manage edit data in database (SQL) """

      # Convert input to form intiger
      id = int(id)
      # Connect to Database
      conn = sqlite3.connect('test.db')
      cur = conn.cursor()
      # Execute command SQL
      cur.execute("UPDATE PRODUCT SET NAME=?, STOCK=?, PRICE=? WHERE ID=?", (name,stock,price,id))
      conn.commit()
      conn.close()

  def create_db(self):
      """ Create database (SQL) """

      # Connect to Database
      conn = sqlite3.connect('test.db')
      print("Open Success")
      # Execute command SQL
      conn.execute('''CREATE TABLE PRODUCT
      (ID INTEGER PRIMARY KEY AUTOINCREMENT,
      NAME           TEXT    NOT NULL,
      STOCK           INT     NOT NULL,
      PRICE           INT     NOT NULL);''')
      print("Success")
      conn.close()




# for debug
if __name__=="__main__":
  root = Tk()
  root.title("Shop")
  root.geometry("560x660")
  menubar = Menu(root)
  filemenu = Menu(menubar, tearoff = 0)
  filemenu.add_separator()
  filemenu.add_command(label = "Exit", command = root.destroy)
  menubar.add_cascade(label = "File", menu = filemenu)
  # Call class
  app = Application(root)
  root.config(menu = menubar)
  root.mainloop()
